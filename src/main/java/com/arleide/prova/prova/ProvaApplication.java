package com.arleide.prova.prova;

import com.arleide.prova.prova.entidade.*;
import com.arleide.prova.prova.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@SpringBootApplication
public class ProvaApplication {

	private static final Logger Log = LoggerFactory.getLogger(ProvaApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(ProvaApplication.class, args);
	}


	@Transactional
	@Bean
	public CommandLineRunner demo(VeiculoService veiculoService,
								  MotoristaService motoristaService,
								  DirigeService dirigeService) {
		return (arg) -> {

			Log.info("--------------------------------");
			Log.info("Inicio - Inserir Motorista");

			Motorista motorista1 = new Motorista();
			motorista1.setNome("Jose Rodrigo");
			motorista1.setSexo("Masculino");
			motorista1.setTelefone("+5599202145874");

			Motorista motorista2 = new Motorista();
			motorista2.setNome("Rosana Alves");
			motorista2.setSexo("Feminino");
			motorista2.setTelefone("+5599981205847");

			Motorista motorista3 = new Motorista();
			motorista3.setNome("Sergio");
			motorista3.setSexo("Masculino");
			motorista3.setTelefone("+5586987410230");

			motoristaService.salvar(motorista1);
			motoristaService.salvar(motorista2);
			motoristaService.salvar(motorista3);

			Log.info("Fim - Inserir Motirista");
			Log.info("--------------------------------");

			Log.info("--------------------------------");
			Log.info("Inicio - Inserir Veiculo");

			Veiculo veiculo1 = new Veiculo();
			veiculo1.setMarca("FIAT");
			veiculo1.setModelo("UNO");

			Veiculo veiculo2 = new Veiculo();
			veiculo2.setMarca("RENAULT");
			veiculo2.setModelo("CLIO");

			Veiculo veiculo3 = new Veiculo();
			veiculo3.setMarca("HUNDAI");
			veiculo3.setModelo("HB20");

			veiculoService.salvar(veiculo1);
			veiculoService.salvar(veiculo2);
			veiculoService.salvar(veiculo3);
			Log.info("Fim - Inserir Veiculo");
			Log.info("--------------------------------");

			Log.info("--------------------------------");
			Log.info("Inicio - Inserir Dirige");

			Dirige dirige1 = new Dirige();
			dirige1.setData(LocalDate.now());
			dirige1.setHora(LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm")));
			dirige1.setMotorista(motorista1);
			dirige1.setVeiculo(veiculo1);
			dirigeService.salvar(dirige1);

			Dirige dirige2 = new Dirige();
			dirige2.setData(LocalDate.now());
			dirige2.setHora(LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm")));
			dirige2.setMotorista(motorista1);
			dirige2.setVeiculo(veiculo2);
			dirigeService.salvar(dirige2);

			Dirige dirige3 = new Dirige();
			dirige3.setData(LocalDate.now());
			dirige3.setHora(LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm")));
			dirige3.setMotorista(motorista1);
			dirige3.setVeiculo(veiculo3);
			dirigeService.salvar(dirige3);

			Dirige dirige4 = new Dirige();
			dirige4.setData(LocalDate.now());
			dirige4.setHora(LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm")));
			dirige4.setMotorista(motorista2);
			dirige4.setVeiculo(veiculo1);
			dirigeService.salvar(dirige4);

			Dirige dirige5 = new Dirige();
			dirige5.setData(LocalDate.now());
			dirige5.setHora(LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm")));
			dirige5.setMotorista(motorista2);
			dirige5.setVeiculo(veiculo2);
			dirigeService.salvar(dirige5);

			Dirige dirige6 = new Dirige();
			dirige6.setData(LocalDate.now());
			dirige6.setHora(LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm")));
			dirige6.setMotorista(motorista2);
			dirige6.setVeiculo(veiculo3);
			dirigeService.salvar(dirige6);

			Dirige dirige7 = new Dirige();
			dirige7.setData(LocalDate.now());
			dirige7.setHora(LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm")));
			dirige7.setMotorista(motorista3);
			dirige7.setVeiculo(veiculo1);
			dirigeService.salvar(dirige7);

			Dirige dirige8 = new Dirige();
			dirige8.setData(LocalDate.now());
			dirige8.setHora(LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm")));
			dirige8.setMotorista(motorista3);
			dirige8.setVeiculo(veiculo3);
			dirigeService.salvar(dirige8);


			Dirige dirige9 = new Dirige();
			dirige9.setData(LocalDate.now());
			dirige9.setHora(LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm")));
			dirige9.setMotorista(motorista3);
			dirige9.setVeiculo(veiculo3);
			dirigeService.salvar(dirige9);

			Log.info("Fim - Inserir Dirige");
			Log.info("--------------------------------");

			Log.info("--------------------------------");
			Log.info("Inicio - Excluir Motorista");

		//	motoristaService.deletar(motorista1.getId());

			Log.info("Fim - Excluir Motorista");
			Log.info("--------------------------------");

			Log.info("--------------------------------");
			Log.info("Inicio - Excluir Veiculo");

			veiculoService.deletar(veiculo2.getId());

			Log.info("Fim - Excluir Veiculo");
			Log.info("--------------------------------");

			List<Dirige> listaDirigiPorPeriodo = dirigeService.listarPorPeriodo(LocalDate.of(2023, 10, 22),LocalDate.of(2023, 10, 22));
			List<Veiculo> listaTodosVeiculo = veiculoService.listarTodos();
			List<String> listaMotoritas = motoristaService.listarToodosOrdemAlfabetica();
			Long quantidade = veiculoService.quantidadeVeiculos();

			Log.info("--------------------------------");
			Log.info("Listar todos os registros em Dirige com os respectivos Motoristas e Veículos em um determinado período");
			for(Dirige d : listaDirigiPorPeriodo){
				Log.info("Dirigir: "+ d);
			}

			Log.info("--------------------------------");
			Log.info("Listar todos os veículos: ");

			for(Veiculo v : listaTodosVeiculo){
				Log.info("Veiculo "+"Marca: "+ v.getMarca() + "Modelo: "+ v.getMarca());
			}
			Log.info("--------------------------------");
			Log.info("Listar o Nome dos Motoristas em Ordem Alfabética");
			for(String m : listaMotoritas){
				Log.info("Motorista: "+ m);
			}
			Log.info("--------------------------------");
			Log.info("Listar a Quantidade de Veículos ");

			Log.info("Quantidade Veiculo: " + quantidade);

		};
	}
}

