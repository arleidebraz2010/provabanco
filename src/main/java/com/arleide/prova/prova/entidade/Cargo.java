package com.arleide.prova.prova.entidade;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "cargo")
public class Cargo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "cod_cargo")
    private Integer id;

    @Column(length = 50)
    private String cargo;

    @OneToMany(mappedBy = "cargo", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    private List<Funcionario> funcionarios;

    @Override
    public String toString() {
        return "Cargo{" +
                "id=" + id +
                ", cargo='" + cargo + '\'' +
                ", funcionarios=" + funcionarios +
                '}';
    }
}
