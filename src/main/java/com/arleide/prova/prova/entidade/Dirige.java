package com.arleide.prova.prova.entidade;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "dirige")
public class Dirige {

    public Dirige(Integer id){
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private LocalDate data;
    private String hora;

    @ManyToOne
    @JoinColumn(name = "motorista_id")
    private Motorista motorista;

    @ManyToOne
    @JoinColumn(name = "veiculo_id")
    private Veiculo veiculo;

    @Override
    public String toString() {
        return "Dirige{" +
                "id=" + id +
                ", data=" + data +
                ", hora='" + hora + '\'' +
                ", motorista=" + motorista.getNome() +
                ", veiculo=" + veiculo.getModelo() +
                '}';
    }
}
