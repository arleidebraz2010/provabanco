package com.arleide.prova.prova.entidade;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "funcionario")
public class Funcionario {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "cod_funcionario")
    private Integer id;

    @Column(length = 70)
    private String nome;

    @Column(length = 10)
    private String sexo;

    @Column(length = 20)
    private String telefone;

    @ManyToOne
    @JoinColumn(name = "cod_cargo", nullable = false)
    private Cargo cargo;

    @Override
    public String toString() {
        return "Funcionario{" +
                "id=" + id +
                ", noem='" + nome+ '\'' +
                ", sexo='" + sexo + '\'' +
                ", telefone='" + telefone + '\'' +
                ", cargo=" + cargo.getCargo() +
                '}';
    }
}
