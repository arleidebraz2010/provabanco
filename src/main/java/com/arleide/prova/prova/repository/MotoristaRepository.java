package com.arleide.prova.prova.repository;

import com.arleide.prova.prova.entidade.Motorista;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MotoristaRepository extends JpaRepository<Motorista, Integer> {


    @Query("select m.nome from Motorista m ORDER BY nome ASC")
    public List<String> listaFuncionariosOrdenadosPorNome();

}
