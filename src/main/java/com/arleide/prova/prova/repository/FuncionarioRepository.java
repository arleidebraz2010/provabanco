package com.arleide.prova.prova.repository;

import com.arleide.prova.prova.entidade.Funcionario;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FuncionarioRepository extends JpaRepository<Funcionario, Integer> {

    public List<Funcionario> findAllByOrderByNomeAsc();


}
