package com.arleide.prova.prova.repository;

import com.arleide.prova.prova.entidade.Veiculo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VeiculoRepository extends JpaRepository<Veiculo, Integer> {

}
