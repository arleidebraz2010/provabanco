package com.arleide.prova.prova.repository;

import com.arleide.prova.prova.entidade.Dirige;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface DirigeRepository extends JpaRepository<Dirige, Integer> {

    List<Dirige> findByDataBetween(LocalDate inicio, LocalDate fim);
}
