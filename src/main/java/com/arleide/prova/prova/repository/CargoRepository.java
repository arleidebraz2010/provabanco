package com.arleide.prova.prova.repository;

import com.arleide.prova.prova.entidade.Cargo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CargoRepository extends JpaRepository<Cargo, Integer> {

}
