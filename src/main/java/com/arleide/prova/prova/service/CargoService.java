package com.arleide.prova.prova.service;

import com.arleide.prova.prova.entidade.Cargo;
import com.arleide.prova.prova.repository.CargoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CargoService {

    @Autowired
    private CargoRepository cargoRepository;

    public Cargo salvar(Cargo cargo){
        return cargoRepository.save(cargo);
    }

    public void deletar(Integer id){
        cargoRepository.deleteById(id);
    }

    public List<Cargo> listarTodos(){
        return cargoRepository.findAll();
    }
}
