package com.arleide.prova.prova.service;

import com.arleide.prova.prova.entidade.Motorista;
import com.arleide.prova.prova.repository.MotoristaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class MotoristaService {


    @Autowired
    private MotoristaRepository motoristaRepository;

    public Motorista salvar(Motorista motorista){
        return motoristaRepository.save(motorista);
    }

    @Transactional
    public void deletar(Integer id){
        Motorista motorista = new Motorista(id);
        motoristaRepository.delete(motorista);
    }

    public List<String> listarToodosOrdemAlfabetica(){
        return motoristaRepository.listaFuncionariosOrdenadosPorNome();
    }

}
