package com.arleide.prova.prova.service;

import com.arleide.prova.prova.entidade.Funcionario;
import com.arleide.prova.prova.repository.FuncionarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FuncionarioService {

    @Autowired
    private FuncionarioRepository funcionarioRepository;

    public Funcionario salvar(Funcionario funcionario){
        return funcionarioRepository.save(funcionario);
    }

    public void deletar(Integer id){
        funcionarioRepository.deleteById(id);
    }

    public List<Funcionario> listarTodos(){
        return funcionarioRepository.findAll();
    }

    public List<Funcionario> listarTodosOrdemCrescente(){
        return funcionarioRepository.findAllByOrderByNomeAsc();
    }

    public Long totalFuncionarios(){
        return funcionarioRepository.count();
    }

}
