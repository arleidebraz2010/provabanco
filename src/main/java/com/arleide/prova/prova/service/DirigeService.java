package com.arleide.prova.prova.service;

import com.arleide.prova.prova.entidade.Dirige;
import com.arleide.prova.prova.repository.DirigeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Service
public class DirigeService {

    @Autowired
    private DirigeRepository dirigeRepository;


    public Dirige salvar(Dirige dirige){
        return dirigeRepository.save(dirige);
    }

    public void deletar(Integer id){
        Dirige dirige = new Dirige(id);
        dirigeRepository.delete(dirige);
    }

    public List<Dirige> listarPorPeriodo(LocalDate initio, LocalDate fim){
        return dirigeRepository.findByDataBetween(initio, fim);
    }
}
