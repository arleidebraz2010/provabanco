package com.arleide.prova.prova.service;

import com.arleide.prova.prova.entidade.Dirige;
import com.arleide.prova.prova.entidade.Motorista;
import com.arleide.prova.prova.entidade.Veiculo;
import com.arleide.prova.prova.repository.DirigeRepository;
import com.arleide.prova.prova.repository.MotoristaRepository;
import com.arleide.prova.prova.repository.VeiculoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class VeiculoService {

    @Autowired
    private VeiculoRepository veiculoRepository;

    public Veiculo salvar(Veiculo veiculo){
        return veiculoRepository.save(veiculo);
    }

    public void deletar(Integer id){
        Veiculo veiculo = new Veiculo(id);
        veiculoRepository.delete(veiculo);
    }

    public List<Veiculo> listarTodos(){
        return veiculoRepository.findAll();
    }

    public Long quantidadeVeiculos(){
        return veiculoRepository.count();
    }

}
